package com.example.diogenes.samplesnotification;

import android.app.Notification;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickNotificacaoSimples(View view) {
        int id = 1;
        Intent intent = new Intent(this,ScreenCalledByNotificationActivity.class);
        intent.putExtra("msg","Olá Alunos, como vão?");
        String contentTitle = "Atualização de saldo";
        String contentText = "Chegaram novos bitcoin em sua conta";
        NotificationUtil.criarNotificacaoSimples(this,intent,contentTitle,contentText,id);
    }


    public void onClickNotificacaoGrande(View view) {
        int id = 2;
        Intent intent = new Intent(this,ScreenCalledByNotificationActivity.class);
        intent.putExtra("msg","Olá investidor, como vai?");
        List<String> list = new ArrayList<>();
        list.add("BTC - 30.000");
        list.add("ETH - 10.000");
        list.add("LTC - 20.000");
        String contentTitle = "Saldo da carteira";
        String contentText = String.format("Você possui %s novos valores",list.size());
        NotificationUtil.criarNotificacaoGrande(this, intent, contentTitle, contentText, list, id);
    }

    public void onClickNotificacaoComAcao(View view){
        int id = 3;
        Intent intent = new Intent(this,ScreenCalledByNotificationActivity.class);
        intent.putExtra("msg","Olá investidor, como vai?");
        String contentTitle = "Decida o que fazer =)";
        String contextText = "Alguma coisa";
        NotificationUtil.criarNotificacaoComAcao(this,intent,contentTitle,contextText,id);
    }

    public void onClickNotificacaoHeadsUp(View view){
        int id = 4;
        Intent intent = new Intent(this,ScreenCalledByNotificationActivity.class);
        intent.putExtra("msg","Olá investidor, como vai?");
        String contentTitle = "Atenção";
        String contextText = "Bitcoins Expirando!!!";
        NotificationUtil.criarHeadsUpNotification(this,intent,contentTitle,contextText,id);
        NotificationUtil.createPrivateNotification(this,intent,contentTitle,contextText,id);
    }

    public void onClickNotificacaoComImagens(View view){
        int id = 5;
        Intent intent = new Intent(this, ScreenCalledByNotificationActivity.class);
        String imageTitle = "Titulo";
        String imageDescription = "Descrição da Imagem";
        NotificationUtil.criarNotificacaoComImagem(this,intent,imageTitle, imageDescription,id);
    }

    public void onClickNotificacaoPrivada(View view){
        int id = 6;
        Intent intent = new Intent(this,ScreenCalledByNotificationActivity.class);
        intent.putExtra("msg","Olá investidor, como vai?");
        String contentTitle = "Atenção";
        String contextText = "Algo a ser mostrado";
        NotificationUtil.createPrivateNotification(this,intent,contentTitle,contextText,id);
    }
}
